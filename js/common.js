$(document).ready(function() {
    $('.promo1-note-off').on('click', function(e){
        e.preventDefault();
        $(this).removeClass('active');
        $('.promo1-note-text').hide();
        $('.promo1-note-on').addClass('active');
    })
    $('.promo1-note-on').on('click', function(e){
        e.preventDefault();
        $(this).removeClass('active');
        $('.promo1-note-text').show();
        $('.promo1-note-off').addClass('active');
    })
    $('.content-cars .car-list .car-item').on('click', function(){
        var car_popup = $('#car-popup');
        car_popup.find('.car-img img').attr('src', $(this).data('img'));
        car_popup.find('.car-name').html($(this).find('.car-name').html());
        $.fancybox.open({href: '#car-popup', wrapCSS: 'modal-car'});
    })
    
    $('.menu-toggle').on('click', function(){
        $('.header-menu').toggle();
    })
})

function j7_modal(id){
    $.fancybox.open({href: '#' + id, wrapCSS: 'modal-'+id, 'minWidth': 515});
}


